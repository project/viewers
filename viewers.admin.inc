<?php

/*
 * @file
 * viewers.admin.inc
 */

function viewers_admin_settings() {

  $form['viewers_time'] = array(
    '#title' => t('Maximum time a user appears on the viewers list'),
    '#description' => t('Time is in seconds. Default is 60 secs.'),
    '#size' => '4',
    '#type' => 'textfield',
    '#default_value' => variable_get('viewers_time', '60'),
  );

  $form['viewers_pager'] = array(
    '#title' => t('Do you want to support pager'),
    '#description' => t('For example, do you want a page (node/100?page=1) to be different from its next page (node/100?page=2).If yes the module will display online users per page not per topic. Default is Yes.'),
    '#type' => 'radios',
    '#default_value' => variable_get('viewers_pager', 'yes'),
    '#options' => array(
      'yes' => t('Yes'),
      'no' => t('No'),
    ),
  );

  $form['viewers_users'] = array(
    '#title' => t('Select which users roles to display'),
    '#type' => 'checkboxes',
    '#description' => t('Default is none. If no user roles are checked, the block doesn\'t show up.'),
    '#default_value' => variable_get('viewers_users', array()),
    '#options' => user_roles(),
  );

  $form['viewers_display'] = array(
    '#title' => t('Display block if there aren\'t users online'),
    '#type' => 'radios',
    '#description' => t('For example you choosed that you want to show only <b>anonymous</b> users, if no anonymous users are online, what you want to do with the block? Default is no.<br/> <strong>Attention:</strong> When you have enabled tracking to specific pages, the block will appear only to them.'),
    '#default_value' => variable_get('viewers_display', 'no'),
    '#options' => array(
      'yes' => t('Yes'),
      'no' => t('No'),
    ),
  );

  $form['viewers_users_exclude'] = array(
    '#title' => t('Which users to exclude'),
    '#type' => 'textarea',
    '#description' => t('Seperate uids with a comma, for example 1,2,34 etc. If by mistake you wrote the same uid twice, the validation saves only the first one. <br/><b>Attention:</b> Don\'t leave a spacebar char at the end of the uids.'),
    '#default_value' => variable_get('viewers_users_exclude', ''),
  );

  // Page specific visibility configurations.
  $access = user_access('use PHP for tracking visibility');
  $visibility = variable_get('viewers_visibility', 0);
  $pages = variable_get('viewers_pages', '');

  if (!$access) {
    $form['visibility'] = array('#type' => 'value', '#value' => 2);
    $form['pages'] = array('#type' => 'value', '#value' => $pages);
  }
  else {
    $options = array(t('Add to every page except the listed pages.'), t('Add to the listed pages only.'));
    $description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));

    if ($access) {
      $options[] = t('Add if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).');
      $description .= ' '. t('If the PHP-mode is chosen, enter PHP code between %php. Note that executing incorrect PHP-code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    $form['viewers_visibility'] = array(
      '#type' => 'radios',
      '#title' => t('Add tracking to specific pages'),
      '#options' => $options,
      '#default_value' => $visibility,
    );
    $form['viewers_pages'] = array(
      '#type' => 'textarea',
      '#title' => t('Pages'),
      '#default_value' => $pages,
      '#description' => $description,
      '#wysiwyg' => FALSE,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings')
  );

  return $form;
}

/*
 * Advanced settings for theming purposes
 */

function viewers_advanced_settings() {
  $status = variable_get('viewers_users', '');
  //if the user has enabled and install colorpicker module add support for it:
  if (module_exists('colorpicker')) {
    foreach (user_roles() as $user => $name) {
      if ($status[$user] != 0 && $user != 1) {
        $form['viewers_user_color'][$user] = array(
          '#title' => $name,
          '#type' => (function_exists('colorpicker_2_or_later') ? 'colorpicker_' : '') .'textfield',
          '#description' => t('Only the selected roles are available.Default is #000000. Please enter a color value in the form #RRGGBB'),
          '#default_value' => variable_get('viewers_user_color_'. $user, '#000000'),
        );
      }
    }
  }

  $form['viewers_separator'] = array(
    '#title' => t('Choose separator to separate users'),
    '#type' => 'radios',
    '#default_value' => variable_get('viewers_separator', ','),
    '#options' => array(
      ',' => 'comma ( , )',
      '-' => 'hyphen ( - )',
      '/' => 'slash ( / )',
      ' ' => 'space (  )',
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings')
  );

  return $form;
}

function viewers_advanced_settings_submit($form, &$form_state) {

  //set the variables
  variable_set('viewers_separator', $form_state['values']['viewers_separator']);
  foreach (user_roles() as $user => $name) {
    if ($form_state['values'][$name] != '#000000') {
      variable_set('viewers_user_color_'. $user, $form_state['values'][$user]);
    }
  }
}

function viewers_admin_settings_validate($form, &$form_state) {
  //validate viewers_users_exclude
  //todo use db_query_range
  $max_uid = db_result(db_query('SELECT uid FROM {users} ORDER BY uid DESC LIMIT 0,1'));
  $users = explode(',', $form_state['values']['viewers_users_exclude']);
  $unique = array_unique($users);
  if (empty($users)) {
    foreach ($users as $uid) {
      if (!ctype_digit($uid) || ($uid < 0) || ($uid > $max_uid)) {
        form_set_error('viewers_users_exclude', t('Only valid uids are allowed'));
      }
    }
  }
  //if uids appeared multiple times, save unique uids only once
  $form_state['values']['viewers_users_exclude'] = implode(',', $unique);

  //validate viewers_time
  if (!ctype_digit($form_state['values']['viewers_time'])) {
    form_set_error('viewers_time', t('Only numbers are allowed.'));
  }
  elseif ($form_state['values']['viewers_time'] <= 0) {
    form_set_error('viewers_time', t('Time must be greater than zero.'));
  }

  // Trim some text area values.
  $form_state['values']['viewers_pages'] = trim($form_state['values']['viewers_pages']);

}

function viewers_admin_settings_submit($form, &$form_state) {
  //if the configuration has changed delete the non-selected roles
  $sync = viewers_admin_settings_refresh_data($form_state['values']['viewers_users']);
  if ($sync) drupal_set_message(t('Tables have been synchronized.'));

  //set the variables
  variable_set('viewers_time', $form_state['values']['viewers_time']);
  variable_set('viewers_pager', $form_state['values']['viewers_pager']);
  variable_set('viewers_users', $form_state['values']['viewers_users']);
  variable_set('viewers_display', $form_state['values']['viewers_display']);
  variable_set('viewers_users_exclude', $form_state['values']['viewers_users_exclude']);
  variable_set('viewers_pages', $form_state['values']['viewers_pages']);
  variable_set('viewers_visibility', $form_state['values']['viewers_visibility']);

  //delete the excluded users from the database after submit
  $message = viewers_admin_settings_exclude_users($form_state['values']['viewers_users_exclude']);
  if ($message != 0) {
    drupal_set_message(t($message .' users excluded.'));
    variable_set('viewers_users_exclude', $form_state['values']['viewers_users_exclude']);
  }
  else {
    variable_set('viewers_users_exclude', '');
  }
  drupal_set_message(t('Your settings have been saved.'));

}

/*
 * Delete the excluded users from the database
 */

function viewers_admin_settings_exclude_users($users) {
  $count = 0;
  $users = explode(',', $users);
  foreach ($users as $user => $uid) {
    //only count assigned uids
    if ($uid) {
      db_query('DELETE FROM {viewers} WHERE uid = %d', $uid);
      $count++;
    }
  }
  return $count;
}

/*
 * If the users changed its preferences delete the non-checked roles
 * If not return false
 */

function viewers_admin_settings_refresh_data($new_settings) {
  $users = variable_get('viewers_users', '');
  $flag = FALSE;
  foreach ($new_settings as $role => $status) {
    if ($users[$role] != $status && $status == 0) {
      db_query('DELETE viewers.* FROM {viewers} INNER JOIN {users_roles} ON users_roles.uid = viewers.uid WHERE rid = %d', $role);
      $flag = TRUE;
    }
  }
  return $flag;
}